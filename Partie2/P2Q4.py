
import cv2
import os
from matplotlib import pyplot as plt


#Chargement d'une image (imread)
img_boat = cv2.imread('../data/boats.jpg')
img_brique = cv2.imread('../data/brique.jpg')

# zebre_img_gr = cv2.cvtColor(zebre_img, cv2.COLOR_BGR2GRAY)

#Application de l'opérateur Laplacien afin d'accentuer les effets de contour (verticaux et horizontaux)
laplacian = cv2.Laplacian(img_brique,cv2.CV_64F)

#Application de l'opérateur Sobel afin d'accentuer les traits verticaux
sobelx = cv2.Sobel(img_brique, cv2.CV_64F,1,0,ksize=3)

#Application de l'opérateur Sobel afin d'accentuer les traits horizontaux
sobely = cv2.Sobel(img_brique,cv2.CV_64F,0,1,ksize=3)

#Application du filtre de Canny pour la détéction de contours
canny = cv2.Canny(img_brique, 100, 200)

#Affichage de l'image ainsi que des différentes opérations réalisées sur cette image
plt.subplot(2,2,1),plt.imshow(canny,cmap = 'gray')
plt.title('Filtre Canny'), plt.xticks([]), plt.yticks([])

plt.subplot(2,2,2),plt.imshow(sobelx,cmap = 'gray')
plt.title('Filtre Sobel vertical'), plt.xticks([]), plt.yticks([])

plt.subplot(2,2,3),plt.imshow(sobely,cmap = 'gray')
plt.title('Filtre Sobel horizontal'), plt.xticks([]), plt.yticks([])

plt.subplot(2,2,4),plt.imshow(laplacian,cmap = 'gray')
plt.title('Filtre Laplacien'), plt.xticks([]), plt.yticks([])
plt.show()