
import cv2
from matplotlib import pyplot as plt

#Chargement d'une image (imread)
zebre_img = cv2.imread('../data/zebre.jpg')
suzan_img = cv2.imread('../data/suzan.jpg')



#Affichage de l'image ainsi que des différentes opérations réalisées sur cette image
plt.subplot(2,2,1),plt.imshow(zebre_img,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])

plt.subplot(2,2,2),plt.imshow(sobely,cmap = 'gray')
plt.title('Filtre Sobel horizontal'), plt.xticks([]), plt.yticks([])

plt.subplot(2,2,3),plt.imshow(suzan_img,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])

plt.subplot(2,2,4),plt.imshow(sobelx,cmap = 'gray')
plt.title('Filtre Sobel vertical'), plt.xticks([]), plt.yticks([])
plt.show()