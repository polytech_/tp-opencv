# Image Processing with OpenCV
import cv2
import os
import sys
import numpy as np
from matplotlib import pyplot as plt


#Version de python : sys.version (détaillée : sys.version_info)
#Version de opencv
#Le répertoire de travail courant : os.getcwd()
print("Python version")
print (sys.version)
print("Version info.")
print (sys.version_info)

print("OPENCV Version =", cv2.__version__)

rep_cour = os.getcwd()
print(rep_cour)

#Chargement d'une image (imread)
#Passage de l'image img en mode gris (cvtColor)
fields = cv2.imread('../data/fields.jpg')
boats = cv2.imread('../data/boats.jpg')
pisarro = cv2.imread('../data/pisarro.jpg')
delphin_dither = cv2.imread('../data/delphin_dither.jpg')
img = cv2.cvtColor(boats, cv2.COLOR_BGR2GRAY)

#Application de l'opérateur Laplacien afin d'accentuer les effets de contour en ne gardant que les contours
# laplacian = cv2.Laplacian(img,cv2.CV_64F)

#Application de l'opérateur Sobel afin d'accentuer les effets de contour en gardant l'image
# sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)
# sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)

#Application d'un filtre médian sur un voisinage 9x9:
median_blur = cv2.medianBlur(img, 9)

#Application d'un filtrage gaussien sur un voisinage 5x5:
gaussian_blur = cv2.GaussianBlur(img, (9, 9), 0)

#Application d'une filtre bilatéral avec un rayon de kernel égal à 9
bilateral_blur = cv2.bilateralFilter(img, 9, 80, 80)

#Affichage de l'image ainsi que des différentes opérations réalisées sur cette image
plt.subplot(1,2,1),plt.imshow(img,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2),plt.imshow(median_blur,cmap = 'gray')
plt.title('Filtre médian'), plt.xticks([]), plt.yticks([])
# plt.subplot(2,2,3),plt.imshow(gaussian_blur,cmap = 'gray')
# plt.title('Filtre Gaussien'), plt.xticks([]), plt.yticks([])
# plt.subplot(2,2,4),plt.imshow(bilateral_blur,cmap = 'gray')
# plt.title('Filtre bilatéral'), plt.xticks([]), plt.yticks([])
plt.show()