import cv2
import numpy as np
import matplotlib.pyplot as plt

#Lecture de l'image "lisa.png"
lisa_img = cv2.imread('../Partie1/Q1/lisa.png')
#Convertion de l'image en niveau de gris
lisa_img_grey = cv2.cvtColor(lisa_img, cv2.COLOR_BGR2GRAY)

#Enregistrement de l'image en niveau de gris
cv2.imwrite("lisa_gris.png", lisa_img_grey)

#Application du filtre moyenneur blur avec les différentes tailles de kernel:
lisa_filter_5x5 = cv2.blur(lisa_img_grey, (5, 5))
lisa_filter_9x9 = cv2.blur(lisa_img_grey, (9, 9))
lisa_filter_15x15 = cv2.blur(lisa_img_grey, (15, 15))
lisa_filter_all = cv2.blur(lisa_img_grey, (494, 494))

#Enregistrement des images dans le dossier filter
cv2.imwrite("filter/lisa_filter_5x5.png", lisa_filter_5x5)
cv2.imwrite("filter/kernel_9x9.png", lisa_filter_9x9)
cv2.imwrite("filter/kernel_15x15.png", lisa_filter_15x15)
cv2.imwrite("filter/kernel_all.png", lisa_filter_all)

#Affichage des images floutées afin de comparer avec l'originale
plt.subplot(121)
plt.imshow(lisa_img_grey)
plt.title('Lisa en niveaux de gris')
plt.xticks([])
plt.yticks([])

plt.subplot(122)
plt.imshow(lisa_filter_9x9)
plt.title('Application du filtre moyenneur')
plt.xticks([])
plt.yticks([])
plt.show()