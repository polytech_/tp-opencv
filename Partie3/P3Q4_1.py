import cv2

# Fonction de dilatation de l'image frame
def dilatation(frame):
    dilatation_size = 2
    dilation_shape = cv2.MORPH_RECT
    element = cv2.getStructuringElement(dilation_shape, (2 * dilatation_size + 1, 2 * dilatation_size + 1),
                                       (dilatation_size, dilatation_size))
    dilatation_dst = cv2.dilate(frame, element)
    return dilatation_dst

#Fonction d'érosion de l'image frame
def erosion(frame):
    erosion_size = 4
    erosion_shape = cv2.MORPH_ELLIPSE
    element = cv2.getStructuringElement(erosion_shape, (2 * erosion_size + 1, 2 * erosion_size + 1),
                                       (erosion_size, erosion_size))
    erosion_dst = cv2.erode(frame, element)
    return erosion_dst


#Chargement de l'image
cellules = cv2.imread('../data/cellules.png')

#Conversion en niveau de gris
cellules = cv2.cvtColor(cellules, cv2.COLOR_BGR2GRAY)

#Binarisation de l'image
ret, thresh = cv2.threshold(cellules, 80, 255, cv2.THRESH_BINARY)

#Appel de connectedComponentsWithStats qui nous permettra d'avoir les informations voulues
output = cv2.connectedComponentsWithStats(thresh, connectivity=4)

# Résultat
# Nombre de labels à l'indice 0 du tableau output
num_labels = output[0]

# Affichage du nombre de labels (cellules) trouvé
print("Cellules trouvées :", num_labels)