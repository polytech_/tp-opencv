# Mon script OpenCV : Video_processing
#
import numpy as np
import cv2

#Fonction de dilatation de l'image frame
def dilatation(frame):
    dilatation_size = 2
    dilation_shape = cv2.MORPH_RECT
    element = cv2.getStructuringElement(dilation_shape, (2 * dilatation_size + 1, 2 * dilatation_size + 1),
                                       (dilatation_size, dilatation_size))
    dilatation_dst = cv2.dilate(frame, element)
    return dilatation_dst

#Fonction d'érosion de l'image frame
def erosion(frame):
    erosion_size = 2
    erosion_shape = cv2.MORPH_RECT
    element = cv2.getStructuringElement(erosion_shape, (2 * erosion_size + 1, 2 * erosion_size + 1),
                                       (erosion_size, erosion_size))
    erosion_dst = cv2.erode(frame, element)
    return erosion_dst


#Chargement de la vidéo jurassicworld.mp4
cap = cv2.VideoCapture('../data/jurassicworld.mp4')

#Stockage de l'image qui précède l'image en cours dans la boucle
ret_previous,previous = cap.read()

#Capture image par image
while (True):

    #Capture de l'image en cours
    ret, frame = cap.read()

    #Une fois la lecture réussie
    if ret == True:
        #Copie de l'image en cours dans la variable img
        img = frame.copy()
        #Conversion de cette image en niveau de gris
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        #Binarisation de l'image en cours
        ret_img,img_bin = cv2.threshold(img, 130, 255, cv2.THRESH_BINARY)

        #Différence entre deux images successives
        cv2_diff = cv2.subtract(img, previous)

        #Dilatation de l'image en cours
        img_dilatee = dilatation(img)

        #Erosion de l'image en cours
        img_erodee = erosion(img)

        #Passage à l'image suivante
        pre = img

        # Affichage des résultats
        cv2.imshow('MavideoAvant', img)
        cv2.imshow('MavideoApresDifference', cv2_diff)

        cv2.imshow('MavideoApresDilatation', img_dilatee)
        cv2.imshow('MavideoApresErosion', img_erodee)


    else:
        print('Vidéo terminée')
        break

    if cv2.waitKey(1000) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
