import cv2
from matplotlib import pyplot as plt

#Lecture des images "paysage.jpg" et "soleil.png" et
# un exemple
prison_img = cv2.imread('../data/prison.jpg')
phone_img = cv2.imread('../data/phone.jpg')
lena_img = cv2.imread('../data/lena.jpg')
musc_img = cv2.imread('../data/musc.jpg')


#Niveau de gris
prison_grey = cv2.cvtColor(prison_img, cv2.COLOR_BGR2GRAY)
phone_grey = cv2.cvtColor(phone_img, cv2.COLOR_BGR2GRAY)
lena_grey = cv2.cvtColor(lena_img, cv2.COLOR_BGR2GRAY)
musc_grey = cv2.cvtColor(musc_img, cv2.COLOR_BGR2GRAY)

#Seuillage fixe (binaire)
retval_prison,thresh_prison = cv2.threshold(prison_grey, 130, 255, cv2.THRESH_BINARY)
retval_phone,thresh_phone = cv2.threshold(phone_grey, 130, 255, cv2.THRESH_BINARY)

retval_lena,thresh_lena = cv2.threshold(lena_grey, 130, 255, cv2.THRESH_BINARY)
retval_lena_inv,thresh_lena_inv = cv2.threshold(lena_grey, 130, 255, cv2.THRESH_BINARY_INV)

retval_musc,thresh_musc = cv2.threshold(musc_grey, 130, 255, cv2.THRESH_BINARY)
ad_thresh_musc_moy = cv2.adaptiveThreshold(musc_grey, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                                           cv2.THRESH_BINARY, 11, 2)
ad_thresh_musc_gauss = cv2.adaptiveThreshold(musc_grey,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                           cv2.THRESH_BINARY,11,2)

#Affichage des résultats
plt.subplot(2,2,1),plt.imshow(musc_grey, cmap ='gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])

plt.subplot(2,2,2),plt.imshow(ad_thresh_musc_moy, cmap ='gray')
plt.title('Adaptatif 1'), plt.xticks([]), plt.yticks([])

plt.subplot(2,2,3),plt.imshow(musc_grey,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])

plt.subplot(2,2,4),plt.imshow(ad_thresh_musc_gauss,cmap = 'gray')
plt.title('Adaptatif 2'), plt.xticks([]), plt.yticks([])
plt.show()

plt.show()

