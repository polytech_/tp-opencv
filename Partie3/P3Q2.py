import cv2
from matplotlib import pyplot as plt
import numpy as np

#Lecture des images
jeu1 = cv2.imread('../data/jeu1.jpg')
jeu2 = cv2.imread('../data/jeu2.jpg')

# #Conversion en niveau de gris
# jeu1_gris = cv2.cvtColor(jeu1, cv2.COLOR_BGR2GRAY)
# jeu2_gris = cv2.cvtColor(jeu2, cv2.COLOR_BGR2GRAY)

#Différence :
difference_1 = cv2.subtract(jeu2, jeu1)
difference_2 = cv2.subtract(jeu1, jeu2)
addition = cv2.bitwise_not(cv2.add(difference_1, difference_2))


#Affichage des résultats
plt.subplot(),plt.imshow(addition, cmap ='gray')
plt.title('Résultat'), plt.xticks([]), plt.yticks([])
#
# plt.subplot(2,2,1),plt.imshow(jeu1, cmap ='gray')
# plt.title('jeu 1'), plt.xticks([]), plt.yticks([])
#
# plt.subplot(2,2,2),plt.imshow(jeu2, cmap ='gray')
# plt.title('jeu 2'), plt.xticks([]), plt.yticks([])
#
# plt.subplot(2,2,3),plt.imshow(difference_1,cmap = 'gray')
# plt.title('différence 1 - 2'), plt.xticks([]), plt.yticks([])
#
# plt.subplot(2,2,4),plt.imshow(difference_2,cmap = 'gray')
# plt.title('différence 2 - 1'), plt.xticks([]), plt.yticks([])
plt.show()


