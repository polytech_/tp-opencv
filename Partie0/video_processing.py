# Mon script OpenCV : Video_processing
#
import numpy as np
import cv2

#
def frame_processing(imgc):
     return imgc

#Chargement de la vidéo jurassicworld.mp4
cap = cv2.VideoCapture('data/jurassicworld.mp4')

#Boucle infinie
while (True):

    #Capture image par image, ici capture de l'image en cours
    ret, frame = cap.read()

    #Une fois la lecture réussie
    if ret == True:
        #Copie de l'image en cours dans la variable img
        img = frame.copy()
        #Conversion de cette image en niveau de gris
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        #Appel de la fonction frame_processing
        gray = frame_processing(gray)

        #Affichage de l'image et de sa verion en niveau de gris
        cv2.imshow('MavideoAvant', frame)
        cv2.imshow('MavideoApres', gray)

    else:
        print('video ended')
        break

    if cv2.waitKey(1000) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
