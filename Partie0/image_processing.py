# Image Processing with OpenCV

import cv2
import os
import sys
import numpy as np
from matplotlib import pyplot as plt


#Version de python : sys.version (détaillée : sys.version_info)
#Version de opencv
#Le répertoire de travail courant : os.getcwd()
print("Python version")
print (sys.version)
print("Version info.")
print (sys.version_info)

print("OPENCV Version =", cv2.version)

rep_cour = os.getcwd()
print(rep_cour)

#Chargement d'une image (imread)
#Passage de l'image img en mode gris (cvtColor)
img = cv2.imread('../data/boats.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#Application de l'opérateur Laplacien afin d'accentuer les effets de contour en ne gardant que les contours
laplacian = cv2.Laplacian(img,cv2.CV_64F)

#Application de l'opérateur Sobel afin d'accentuer les effets de contour en gardant l'image
sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)
sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)

#Affichage de l'image ainsi que des différentes opérations réalisées sur cette image
plt.subplot(2,2,1),plt.imshow(img,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(laplacian,cmap = 'gray')
plt.title('Laplacian'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(sobelx,cmap = 'gray')
plt.title('Sobel X'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(sobely,cmap = 'gray')
plt.title('Sobel Y'), plt.xticks([]), plt.yticks([])
plt.show()