import cv2
from matplotlib import pyplot as plt

#Lecture des images "paysage.jpg" et "soleil.png" et
# un exemple
paysage_img = cv2.imread('paysage.jpg')
soleil_img = cv2.imread('soleil.png')
example_img = cv2.imread('example.png')

#Convertion des images en niveau de gris
paysage_grey = cv2.cvtColor(paysage_img, cv2.COLOR_BGR2GRAY)
soleil_grey = cv2.cvtColor(soleil_img, cv2.COLOR_BGR2GRAY)
example_grey = cv2.cvtColor(example_img, cv2.COLOR_BGR2GRAY)


#Enregistrement des images en niveau de gris
cv2.imwrite("niveau_gris/paysage_grey.jpg", paysage_grey)
cv2.imwrite("niveau_gris/soleil_grey.jpg", soleil_grey)
cv2.imwrite("niveau_gris/example_grey.jpg", example_grey)

#Affichage des histogrammes de niveaux de gris
# plt.hist(paysage_grey.ravel(), 256, [0, 256], label="paysage_grey.png")
plt.hist(soleil_grey.ravel(), 256, [0,256], label="soleil_grey.jpg")
#plt.hist(example_grey.ravel(), 256, [0, 256], label="example_grey.png")

#Egalisation des histogrammes
eg_paysage_grey = cv2.equalizeHist(paysage_grey)
eg_soleil_grey = cv2.equalizeHist(soleil_grey)
eg_example_grey = cv2.equalizeHist(example_grey)

#Affichage des histogrammes de niveaux de gris après égalisation
# plt.hist(eg_paysage_grey.ravel(), 256, [0, 256], label="paysage_grey.png avec égalisation")
plt.hist(eg_soleil_grey.ravel(), 256, [0, 256], label="soleil_grey.jpg après égalisation")
# plt.hist(eg_example_grey.ravel(), 256, [0, 256], label="example_grey.png après égalisation")
plt.legend(loc='upper right')
#
plt.xlabel("Niveau de gris")
plt.ylabel("Nombre de pixels")

#Enregistrement des images en niveau de gris après égalisation
cv2.imwrite("niveau_gris/egalisation/eg_paysage_grey.jpg", eg_paysage_grey)
cv2.imwrite("niveau_gris/egalisation/eg_soleil_grey.jpg", eg_soleil_grey)
cv2.imwrite("niveau_gris/egalisation/eg_example_grey.png", eg_example_grey)



plt.show()

