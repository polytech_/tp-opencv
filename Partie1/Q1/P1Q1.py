import cv2
from matplotlib import pyplot as plt

#Lecture de l'image "lisa.png"
lisa_img = cv2.imread('../../data/lisa.png')
#Convertion de l'image en niveau de gris
lisa_img_grey = cv2.cvtColor(lisa_img, cv2.COLOR_BGR2GRAY)

#Enregistrement de l'image en niveau de gris
cv2.imwrite("lisa_gris.png", lisa_img_grey)

#Affichage de l'histogramme de niveaux de gris
plt.hist(lisa_img_grey.ravel(), 256, [0, 256])
plt.xlabel("Niveau de gris")
plt.ylabel("Nombre de pixels")
plt.show()

